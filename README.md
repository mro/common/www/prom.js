# Promises utilities

**DEPRECATED** This module has been integrated in [@cern/nodash](https://gitlab.cern.ch/mro/common/www/nodash) it should not be used anymore.

This project is meant to provide some convenient methods to play with promises.

It has been developped by en-smm group to get rid of [q](https://www.npmjs.com/package/q) and [bluebird](https://www.npmjs.com/package/bluebird), providing the features that are missing on native Promise objects.

## Examples

```js
const prom = require('prom');
const fs = require('fs');

/* just to have an asynchronuous context */
(async function() {
  // there are more elegant ways to do this, this is just for the example
  function readdir(path) {
    const deferred = makeDeferred();

    fs.readdir(path, (err, ret) => {
      if (err)
        deferred.reject(err);
      else
        deferred.resolve(ret);
    });
    return deferred.promise;
  }

  /* readdir returns a promise */
  await readdir('/tmp');
  
  /* promise will reject after 1sec if not resolved */
  await prom.timeout(readdir('/tmp'), 1000);
  
  const files = await readdir('/tmp')
  // result is not modified by our 'tap'
  .then(prom.tap((ret) => console.log(ret)));

  /* wait for a moment */
  await prom.delay(100);

  await readdir('/tmp')
  .then((ret) => prom.delay(100, ret))
  .then((ret) => {
    // ret is still our result, it went through the delay
    console.log(ret);
  });
}());
```
