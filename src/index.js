// @ts-check
const { noop, has } = require('lodash');

/**
 * @template T=any
 * @return {prom.Deferred<T>}
 */
function makeDeferred() {
  /** @type {prom.Deferred<T>} */
  const deferred = {};
  deferred.promise = new Promise(function(resolve, reject) {
    deferred.isPending = true;
    deferred.resolve = (val) => {
      deferred.isPending = false;
      resolve(val);
    };
    deferred.reject = (err) => {
      deferred.isPending = false;
      reject(err);
    };
  });
  return deferred;
}

/**
 * @brief adds a timeout on deferred
 * @template T=any
 * @param {prom.Deferred<T>} deferred
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
function deferredTimeout(deferred, ms, error) {
  /** @type {NodeJS.Timeout|null} */
  var timeoutId = setTimeout(function() {
    if (!error || typeof error === "string") {
      error = new Error(error || "Timed out after " + ms + " ms");
      error.code = "ETIMEDOUT";
    }
    timeoutId = null;
    const rej = deferred.reject;
    deferred.resolve = noop;
    deferred.reject = noop;
    rej(error);
    if (deferred.abort) {
      deferred.abort();
    }
  }, ms);

  return deferred.promise.finally(() => {
    if (timeoutId) { clearTimeout(timeoutId); }
  });
}

/**
 * @brief adds a timeout on promise
 * @template T=any
 * @param {Promise<T>} prom
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
function promiseTimeout(prom, ms, error) {
  return new Promise(function(resolve, reject) {
    let resolved = false;
    const timeoutId = setTimeout(function() {
      if (!error || typeof error === "string") {
        error = new Error(error || "Timed out after " + ms + " ms");
        error.code = "ETIMEDOUT";
      }
      resolved = true;
      reject(error);
    }, ms);

    prom.then(function(value) {
      if (!resolved) {
        clearTimeout(timeoutId);
        resolve(value);
      }
    }, function(error) {
      if (!resolved) {
        clearTimeout(timeoutId);
        reject(error);
      }
    });
  });
}

/**
 * @brief adds a timeout on promise
 * @template T=any
 * @param {prom.Deferred<T>|Promise<T>} promise
 * @param {number} ms
 * @param {(Error|any)=} error
 * @return {Promise<T>}
 */
function timeout(promise, ms, error) {
  if (has(promise, 'resolve')) {
    return deferredTimeout(/** @type {prom.Deferred<T>} */ (promise),
      ms, error);
  }
  else {
    return promiseTimeout(/** @type {Promise<T>} */ (promise), ms, error);
  }
}

/**
 * @template T=void
 * @param {number} ms
 * @param {T=} ret
 * @return {Promise<T>}
 */
function delay(ms, ret) {
  return new Promise(function(resolve) {
    setTimeout(function() { resolve(ret); }, ms);
  });
}

/**
 * @template T=any
 * @param  {(ret: T) => any} fun
 * @return {(ret: T) => Promise<T>}
 */
function tap(fun) {
  return function(/** @type {T} */ ret) {
    return Promise.resolve(fun(ret))
    .then(() => ret);
  };
}

module.exports = {
  timeout, makeDeferred, delay, tap
};
