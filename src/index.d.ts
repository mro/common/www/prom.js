
export = prom;
export as namespace prom;

declare namespace prom {
  interface Deferred<T=any> {
    promise: Promise<T>;
    isPending: boolean;
    resolve(ret: T): void;
    reject(err: Error|any): void;
    abort?: () => any;
  }
  function makeDeferred<T=any>(): Deferred<T>;

  function timeout<T=any>(
    prom: Promise<T>|Deferred<T>, ms: number, error?: any): Promise<T>;

  function delay<T=void>(ms: number, ret?: T): Promise<T>;

  function tap<T=any>(fun: (ret: T) => any): (ret: T) => Promise<T>
}
