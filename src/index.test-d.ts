// @ts-check
import { expectType } from 'tsd';

import { delay, makeDeferred, tap, timeout } from '.';

/* deferred */
(async function() {
  const deferred = makeDeferred<number>();

  deferred.resolve(42);
  expectType<number>(await deferred.promise);

  deferred.reject(new Error('failure'));

  await timeout(deferred, 100, 'message');
}());

/* delay + tap + timeout */
(async function() {
  expectType<number>(await delay(100, 42));

  const ret = await Promise.resolve('test')
  .then(tap(() => delay(100)));
  expectType<string>(ret);

  expectType<string>(await timeout(Promise.resolve('test'), 100));
}());
